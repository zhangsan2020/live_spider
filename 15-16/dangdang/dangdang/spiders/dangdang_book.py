# -*- coding: utf-8 -*-
import scrapy
from bs4 import BeautifulSoup
from ..items import DdgItem

class DangdangBookSpider(scrapy.Spider):
    #爬虫的唯一标识
    name = 'dangdang_book'
    #爬虫的域名
    allowed_domains = ['dangdang.com']
    start_urls=[]
    for i in range(5):
        url='http://bang.dangdang.com/books/bestsellers/01.00.00.00.00.00-year-2018-0-1-'+str(i+1)
        start_urls.append(url)
    #解析，提取
    def parse(self, response):
        bs_data=BeautifulSoup(response.text,'html.parser')
        #提取包含全部书籍信息的标签bang_list clearfix bang_list_mode
        lis = bs_data.find('ul',class_="bang_list clearfix bang_list_mode").find_all('li')

        for li in lis:
            item = DdgItem()
            item['book_name'] = li.find('div',class_="name").find('a').text
            item['author'] = li.find('div',class_="publisher_info").find('a').text
            item['price_now'] = li.find('span',class_="price_n").text
            item['price_ori'] = li.find('span',class_="price_r").text
            # print(item)
            yield item



        # for data in datas:
        #     item=DdgItem()
        #     #提取书名
        #     book_name=data.find_all('div',class_='name')
        #     for book in book_name:
        #         item['book_name']=book.find_all('a')[0]['title']
        #         item['author']=data.find_all('div',class_="publisher_info")[0].find_all('a')[0]['title']
        #     jiages=data.find_all('div',class_='price')
        #     for jiage in jiages:
        #         item['price']=jiage.find('span',class_='price_n').text
        #         item['price_ori']=jiage.find('p',class_='price_e').text
        #     yield item