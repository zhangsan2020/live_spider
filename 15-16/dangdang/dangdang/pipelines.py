# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import openpyxl

class DangdangPipeline(object):

    def open_spider(self,spider):

        # print('@@@@'*20)
        self.wb = openpyxl.Workbook()
        self.sheet = self.wb.active
        self.sheet.title = 'now'
        self.sheet.append(['author','book_name','price_now','price_ori'])





    def process_item(self, item, spider):
        self.sheet.append(list(item.values()))
        return item

    def close_spider(self,spider):
        self.wb.save('张磊.xlsx')
        self.wb.close()
        print('****'*20)