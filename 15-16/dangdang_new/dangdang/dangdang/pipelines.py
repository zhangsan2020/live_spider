# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

import openpyxl

class DangdangPipeline(object):


    def open_spider(self,spider):

        self.wb = openpyxl.Workbook()
        self.sheet = self.wb.active





    def process_item(self, item, spider):
        print(item)
        self.sheet.append([item['name'],item['date'],item['price']])
        return item

    def close_spider(self,spider):

        self.wb.save('dangdang.xlsx')
        self.wb.close()