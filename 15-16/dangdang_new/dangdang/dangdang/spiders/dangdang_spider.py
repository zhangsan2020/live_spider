# -*- coding: utf-8 -*-
import scrapy
from bs4 import BeautifulSoup
from ..items import DangdangItem


class DangdangSpiderSpider(scrapy.Spider):
    name = 'dangdang_spider'
    allowed_domains = ['dangdang.com']
    start_urls = []
    for i in range(1,26):
        url = 'http://bang.dangdang.com/books/bestsellers/01.41.00.00.00.00-24hours-0-0-1-2%s'%i
        start_urls.append(url)

    def parse(self, response):
        # print(response.text)
        soup = BeautifulSoup(response.text,'html.parser')
        lis = soup.find('ul',class_="bang_list_mode").find_all('li')

        for li in lis:
            item = DangdangItem()
            item['name'] = li.find('div',class_="name").find('a').text
            item['date'] = li.find_all('div',class_="publisher_info")[-1].find('span').text
            item['price'] = li.find('span',class_="price_n").text
            yield item

