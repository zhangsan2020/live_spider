# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

import openpyxl

class GushiciPipeline(object):

    def open_spider(self,spider):

        self.wb = openpyxl.Workbook()
        self.sheet = self.wb.active
        self.sheet.title = 'adf'
        self.sheet.append(['作者信息','诗名','诗内容'])




    def process_item(self, item, spider):

        self.sheet.append([item['author_info'],item['name'],item['shi_info']])
        return item

    def close_spider(self,spider):
        self.wb.save('aaaa.xlsx')
        self.wb.close()