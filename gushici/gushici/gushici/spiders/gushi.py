# -*- coding: utf-8 -*-
import scrapy
from bs4 import BeautifulSoup
from ..items import GushiciItem

class GushiSpider(scrapy.Spider):
    name = 'gushi'
    allowed_domains = ['gushiwen.org']
    start_urls = ['http://gushiwen.org/']

    def parse(self, response):
        # print(response.text)
        html = BeautifulSoup(response.text,'html.parser')
        conts = html.find('div',class_="main3").find('div',class_="left").find_all('div',class_="cont")
        for cont in conts:
            # print(cont)
            item = GushiciItem()
            print('****'*20)
            name = cont.find('a').text
            item['name'] = name
            author_info = cont.find('p',class_='source').text
            item['author_info'] = author_info
            shi_info = cont.find('div',class_='contson').text.strip()
            item['shi_info'] = shi_info
            yield item

            print(name)
            print(author_info)
            print(shi_info)
            print('****'*20)
        try:
            next_page = html.find('a',class_='amore')['href']
            new_page = 'https://www.gushiwen.org%s'%next_page
            print('&&&'*30)
            print(new_page)
            print('&&&'*30)
            yield scrapy.Request(new_page,callback=self.parse)
        except:
            print('没有页码了')

